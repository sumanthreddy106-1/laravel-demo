<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
    public function login(Request $request)
    {
        if ($request->has('_token')) {
            $email = $request['email'];
            $password = $request['password'];
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                return view('home', ['user' => Auth::user()]);
            }else{

            }
        } else {
            echo '1';
            return view('login');
        }

    }

    public function logout(Request $request)
    {
        Auth::logout();
        return view('login');
    }

}
