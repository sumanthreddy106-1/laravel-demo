<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\LoaiTin;

class AjaxController extends Controller
{
    public function getLoaiTin($id)
    {
        $loaitin = LoaiTin::where('idTheLoai', $id)->get();
        $xhtml = '';
        foreach ($loaitin as $value) {
            $xhtml .= "<option value='" . $value->id . "'>" . $value->Ten . "</option>";
        }
        echo $xhtml;
    }
}
