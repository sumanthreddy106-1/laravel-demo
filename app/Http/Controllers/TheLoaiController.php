<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TheLoai;
use Illuminate\Support\Facades\Redirect;

class TheLoaiController extends Controller
{
    public function getDanhSach()
    {
        $theloai = TheLoai::all();
        return view("admin.theloai.danhsach", ['theloai' => $theloai]);
    }

    public function them(Request $request)
    {
        if($request->has('_token')) {
            $this->validate($request,
                [
                    'ten' => 'required|min:3|max:100'
                ],
                [
                    'ten.required' => 'Bạn chưa nhập tên thể loại',
                    'ten.min' => 'Tên thể loại có độ dài từ 3 cho đến 100 ký tự',
                    'ten.max' => 'Tên thể loại có độ dài từ 3 cho đến 100 ký tự'
                ]);
            $theloai = new TheLoai();
            $theloai->Ten = $request->ten;
            $theloai->TenKhongDau = changeTitle($request->ten);
            $theloai->save();
            return redirect('admin/theloai/them')->with('thongbao', 'Thêm mới thành công');
        }else{
            return view("admin.theloai.them");
        }

    }

    public function sua(Request $request,$id)
    {
        $theloai = TheLoai::find($id);
        if($request->has('_token')) {
            $this->validate($request, [
                'ten' => 'bail|required|unique:TheLoai,Ten|min:3|max:100'
            ],
                [
                    'ten.required' => 'Bạn chưa nhập tên thể loại',
                    'ten.unique' => 'Tên thể loại đã tồn tại',
                    'ten.min' => 'Tên thể loại có độ dài từ 3 cho đến 100 ký tự',
                    'ten.max' => 'Tên thể loại có độ dài từ 3 cho đến 100 ký tự'
                ]);
            $theloai->Ten = $request->ten;
            $theloai->TenKhongDau = changeTitle($request->ten);
            $theloai->Save();
            return redirect('admin/theloai/sua/' . $id)->with('thongbao', 'Sửa thành công');
        }else{
            return view("admin.theloai.sua", ['theloai' => $theloai]);
        }

    }

    public function getXoa($id)
    {
        $theloai = TheLoai::find($id);
        $theloai->delete();
        return Redirect::to('admin/theloai/danhsach')->with('thongbao', 'Bạn đã xóa thành công');
    }
}
