<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TinTuc extends Model
{
    //use SoftDeletes;
    protected $table = "tintuc";

    public function loaiTin()
    {
        return $this->belongsTo('App\Model\LoaiTin', 'idLoaiTin', 'id');
    }

    public function comment()
    {
        return $this->hasMany('App\Model\Comment', 'idTinTuc', 'id');
    }

    protected $dates = ['deleted_at'];
}
