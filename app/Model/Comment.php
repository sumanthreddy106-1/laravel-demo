<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table = "comment";

    public function tinTuc()
    {
        return $this->belongsTo('App\Model\TinTuc', 'idTinTuc', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'idUser', 'id');
    }

}
