<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoaiTin extends Model
{
    use SoftDeletes;
    //
    protected $table = "loaitin";

    public function theLoai()
    {
        return $this->belongsTo('App\Model\TheLoai', 'idTheLoai', 'id');
    }

    public function tinTuc()
    {
        return $this->hasMany('App\Model\Tintuc', 'idLoaiTin', 'id');
    }

    protected $dates = ['deleted_at'];
}
